#!/bin/bash -ex

if [ $(grep -ci $CUPSADMIN /etc/shadow) -eq 0 ]; then
    useradd -r -G lpadmin -M $CUPSADMIN

    # add password
    echo $CUPSADMIN:$CUPSPASSWORD | chpasswd
fi

sed -i 's/^.*enable-dbus=.*/enable-dbus=no/' /etc/avahi/avahi-daemon.conf
sed -i "s/^.*publish-aaaa-on-ipv4=.*/publish-aaaa-on-ipv4=yes/" /etc/avahi/avahi-daemon.conf

# # restore default cups config in case user does not have any
# if [ ! -f /etc/cups/cupsd.conf ]; then
#     cp -rpn /etc/cups-bak/* /etc/cups/
# fi

/usr/sbin/avahi-daemon --daemonize

exec /usr/sbin/cupsd -f